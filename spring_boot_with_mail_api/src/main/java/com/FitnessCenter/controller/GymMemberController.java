package com.FitnessCenter.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FitnessCenter.entity.GymMemberEntity;
import com.FitnessCenter.model.GymMemberDto;
import com.FitnessCenter.model.MsgDto;
import com.FitnessCenter.repository.GymMemberRepository;
import com.FitnessCenter.service.CommonMsgService;
import com.FitnessCenter.service.GymMemberRegistorSevice;

@RestController
public class GymMemberController {
	@Autowired GymMemberRegistorSevice userRegistorService;
	@Autowired GymMemberRepository userRepository;
	@Autowired CommonMsgService commonMsgService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public  String createUser(@RequestBody GymMemberDto UserDto) throws Exception {	
		GymMemberEntity userExist = userRegistorService.findByEmail(UserDto.getEmail());
		if(userExist != null) {
			return "Email is  already in used";
		}
		boolean user  = userRegistorService.submitUserDetail(UserDto);
		if(user) 
			return "successMessage:  Registration successfully";  
		else
			return "failureMessage:";

	}
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public ResponseEntity<?> loginUser(@RequestBody UserDto userInfo) {
//		UserEntity user = userRepository.findByEmailAndPassword(userInfo.getEmail(), userInfo.getPassword());
//		if(null == user) {
//
//			return new ResponseEntity<>("Email and password  does not exit", HttpStatus.BAD_REQUEST);
//		}else {
//			return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
//		}
//	}

	@RequestMapping(value="/getMemberDetails", method=RequestMethod.GET)
	public ResponseEntity<?> getMemberDetails(){
		java.util.List<GymMemberEntity> member = userRepository.findAll();
        return new ResponseEntity<>(member, HttpStatus.OK); 
		
	}
	
	@RequestMapping(value="/deleteGymMemberById", method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteGymMember(@RequestParam(name="id") Long id){
		 userRepository.deleteById(id);
		
        return new ResponseEntity<>( HttpStatus.OK); 
		
	}
	
	
	@RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
	public  String userMessage(@RequestBody MsgDto msgDto) throws Exception {	
		
		boolean msg  = commonMsgService.sendMessage(msgDto);
		if(msg) 
			return "successMessage: Message Send successfully";  
		else
			return "failureMessage:";
	}
	
}
