package com.FitnessCenter.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.FitnessCenter.entity.GymTrainerEntity;
import com.FitnessCenter.model.GymTrainerDto;
import com.FitnessCenter.repository.FileUploadRepository;
import com.FitnessCenter.service.GymTrainerRegistorService;


@RestController
public class GymTrainerController {
	
	@Autowired GymTrainerRegistorService gymTrainerRegistorService;
	@Autowired FileUploadRepository fileUploadRepository ;
	
	@RequestMapping(value = "/trainerRegistor", method = RequestMethod.POST)
	public  String createUser(@RequestBody GymTrainerDto trainerDto) throws Exception {	
		
		boolean trainer  = gymTrainerRegistorService.submitTrainerDetail(trainerDto);
		if(trainer) 
			return "successMessage:  Registration successfully";  
		else
			return "failureMessage:";

	}
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public String uploadFile(@RequestParam("uploadfile") MultipartFile uploadfile ) {
		boolean upload =gymTrainerRegistorService.uploadFile(uploadfile);
		if(upload)
			return "successMessage:  Registration successfully";  
		else
			return "failureMessage:";
	}

	
	
}
