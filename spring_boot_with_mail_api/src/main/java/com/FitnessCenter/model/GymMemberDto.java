package com.FitnessCenter.model;

public class GymMemberDto {
		String name;
		String email;
		String mobile;
		int membership;
		String timeSlot;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public int getMembership() {
			return membership;
		}
		public void setMembership(int membership) {
			this.membership = membership;
		}
		public String getTimeSlot() {
			return timeSlot;
		}
		public void setTimeSlot(String timeSlot) {
			this.timeSlot = timeSlot;
		}
		
}
