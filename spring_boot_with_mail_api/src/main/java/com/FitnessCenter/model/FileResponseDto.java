package com.FitnessCenter.model;

public class FileResponseDto {

	byte[] content;
	String fileName;
	
	public FileResponseDto() { }

	public FileResponseDto(byte[] content, String fileName) {
		super();
		this.content = content;
		this.fileName = fileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
