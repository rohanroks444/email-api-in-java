package com.FitnessCenter.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.FitnessCenter.entity.FileUploadEntity;
import com.FitnessCenter.entity.GymTrainerEntity;
import com.FitnessCenter.model.GymTrainerDto;
import com.FitnessCenter.repository.FileUploadRepository;
import com.FitnessCenter.repository.GymTrainerRepository;



@Service
public class GymTrainerRegistorService {

	@Autowired GymTrainerRepository gymTrainerRepository;
	@Autowired private Environment env;

	@Autowired FileUploadRepository fileUploadRepository;
	
		
//	
//	public boolean submitTrainerDetail(GymTrainerDto trainerDto,  MultipartFile[] uploadfileList) {
//		
//		GymTrainerEntity trainerEntities = new GymTrainerEntity();
//		trainerEntities.setName(trainerDto.getName());
//		trainerEntities.setEmail(trainerDto.getEmail());
//		trainerEntities.setMobile(trainerDto.getMobile());
//		GymTrainerEntity gymTrainer= gymTrainerRepository.save(trainerEntities);
//		
//		try {
//			Arrays.asList(uploadfileList).forEach(uploadfile -> {
//
//				String filename = uploadfile.getOriginalFilename().replaceAll(" ", "");
//				String directory = env.getProperty("Fitness.paths.uploadedFiles");
//				String filepath = Paths.get(directory, filename).toString();	   
//				
//				BufferedOutputStream stream;
//				try {
//					stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
//					stream.write(uploadfile.getBytes());
//					stream.close();
//				} catch (FileNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//					
//
//					FileUploadEntity fileUploadEntity = new FileUploadEntity();
//					fileUploadEntity.setFileName(filename);
//					fileUploadEntity.setFilePath(filename);
//					fileUploadEntity.setFileType(uploadfile.getContentType());
//					fileUploadEntity.setGymTrainerEntity(gymTrainer);
//					fileUploadRepository.save(fileUploadEntity);
//				
//
//			});
//		} catch (Exception e) {
//			e.printStackTrace();
//
//		}
//		
//		return true;
//	
//	}
	
public boolean submitTrainerDetail(GymTrainerDto trainerDto) {
		
		GymTrainerEntity trainerEntities = new GymTrainerEntity();
		trainerEntities.setName(trainerDto.getName());
		trainerEntities.setEmail(trainerDto.getEmail());
		trainerEntities.setMobile(trainerDto.getMobile());
	    gymTrainerRepository.save(trainerEntities);
	    
		return true;
}

public boolean uploadFile(MultipartFile uploadFile) {
	try {
		Arrays.asList(uploadFile).forEach(uploadfile -> {

			String filename = uploadfile.getOriginalFilename().replaceAll(" ", "");
			String directory = env.getProperty("Fitness.paths.uploadedFiles");
			String filepath = Paths.get(directory, filename).toString();	   
			
			BufferedOutputStream stream;
			try {
				stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
				stream.write(uploadfile.getBytes());
				stream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				

				FileUploadEntity fileUploadEntity = new FileUploadEntity();
				fileUploadEntity.setFileName(filename);
				fileUploadEntity.setFilePath(filename);
				fileUploadEntity.setFileType(uploadfile.getContentType());
				
				fileUploadRepository.save(fileUploadEntity);
			

		});
	} catch (Exception e) {
		e.printStackTrace();

	}
	
	return true;

}


	public GymTrainerEntity findByEmail(String email) {
		
		return gymTrainerRepository.findByEmail(email);
	}
	
}
