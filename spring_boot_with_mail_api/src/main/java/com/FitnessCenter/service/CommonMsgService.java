package com.FitnessCenter.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.FitnessCenter.entity.GymMemberEntity;
import com.FitnessCenter.model.GymMemberDto;
import com.FitnessCenter.model.MsgDto;

@Service
public class CommonMsgService {
	@Autowired
    private JavaMailSender javaMailSender;
	public boolean sendMessage(MsgDto msgDto) {
		
		
		CompletableFuture.runAsync(() -> {
		   SimpleMailMessage msg = new SimpleMailMessage();
		   
	        msg.setTo("abc@gmail.com");
	       
	        msg.setSubject("Query For Fitness Gym");
	        msg.setText("Hello, My name is  "+msgDto.getName()+
	        		"\n\nMessage: "+msgDto.getMessage()+
	        		"\n\nMy Email: "+msgDto.getEmail()+
	        		"\n\nMy Contact Number: "+msgDto.getMobile());

	        javaMailSender.send(msg);
		});
		return true;
	}
	
}
