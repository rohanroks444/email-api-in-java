package com.FitnessCenter.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.FitnessCenter.entity.GymMemberEntity;
import com.FitnessCenter.model.GymMemberDto;
import com.FitnessCenter.repository.GymMemberRepository;

@Service
public class GymMemberRegistorSevice {
	
	@Autowired GymMemberRepository userRepository;
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	public boolean submitUserDetail(GymMemberDto userRegistration) {
		GymMemberEntity userEntities = new GymMemberEntity();
		userEntities.setName(userRegistration.getName());
		userEntities.setEmail(userRegistration.getEmail());
		userEntities.setMobile(userRegistration.getMobile());
		userEntities.setMembership(userRegistration.getMembership());
		userEntities.setTimeSlot(userRegistration.getTimeSlot());
		userRepository.save(userEntities);
		
		CompletableFuture.runAsync(() -> {
		   SimpleMailMessage msg = new SimpleMailMessage();
		   
	        msg.setTo(userRegistration.getEmail());
	       
	        msg.setSubject("Thank You For Joining Fitness Center");
	        msg.setText("Hello,  "+userRegistration.getName()+
	        		"\n\nYour Membership: "+userRegistration.getMembership()+
	        		"Rs\n\nYour Timing: "+userRegistration.getTimeSlot()+
	        		"\n\nYour Contact Number: "+userRegistration.getMobile());

	        javaMailSender.send(msg);
		});
		return true;
	}
	
	public GymMemberEntity findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
}
