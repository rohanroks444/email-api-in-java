package com.FitnessCenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FitnessCenter.entity.GymMemberEntity;

public interface GymMemberRepository extends JpaRepository<GymMemberEntity, Long> {

	GymMemberEntity findByEmail(String email);
	
}
