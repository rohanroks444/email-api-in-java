package com.FitnessCenter.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.FitnessCenter.entity.GymTrainerEntity;

public interface GymTrainerRepository extends JpaRepository<GymTrainerEntity, Long> {

	GymTrainerEntity findByEmail(String email);

}
