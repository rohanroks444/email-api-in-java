package com.FitnessCenter.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="trainer")
public class GymTrainerEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "trainerId")
	private Long id;
	
	@Column(name="Name")
	String name;
	
	@Column(name="Email")
	String email;
	
	@Column(name="Mobile_No")
	String mobile;
	
	
	@OneToMany(mappedBy = "gymTrainerEntity")
	private List<FileUploadEntity> fileUploadEntity;
	

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public List<FileUploadEntity> getFileUploadEntity() {
		return fileUploadEntity;
	}

	public void setFileUploadEntity(List<FileUploadEntity> fileUploadEntity) {
		this.fileUploadEntity = fileUploadEntity;
	}


}
